
# customized gVIM for Windows

this is my customized version of gVIM partable for Microsoft Windows

## installation

download the file

```powershell
Invoke-WebRequest -Uri https://gitlab.com/24F431/gVIM/-/raw/master/gVIM.7z -OutFile gVIM.7z
```

unpack the 7z-file in any folder, thats it

## preview of gVIM

![gVIM preview](./gVIM preview image.png)
