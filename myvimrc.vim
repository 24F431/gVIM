"        _                    
" __   _(_)_ __ ___  _ __ ___ 
" \ \ / / | '_ ` _ \| '__/ __|
"  \ V /| | | | | | | | | (__ 
"   \_/ |_|_| |_| |_|_|  \___|
                             
let mapleader=" "
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set hidden
set ignorecase
set incsearch
set scrolloff=8
" set signcolumn=yes

filetype plugin indent on
set rnu
set laststatus=2
"set colorcolumn=80
nnoremap <leader>c :execute "set colorcolumn="
    \ . (&colorcolumn == "" ? "80" : "")<CR>
highlight ColorColumn ctermbg=DarkMagenta
" V-BLOCK remap
nnoremap <leader>v <c-v>

" copy + paste clipboard
vnoremap <C-k> "+y
map <C-e> "+P
" map undo search highlight
map <leader>h :let @/ = ""<CR>
" split and filemanager
nnoremap <leader>pv :wincmd v<bar> :Ex <bar> :vertical resize 30<cr>
" yank to the end
nnoremap Y y$
" moving text
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
inoremap <C-j> <esc>:m .+1<CR>==
inoremap <C-k> <esc>:m .-2<CR>==
nnoremap <leader>j :m .+1<CR>==
nnoremap <leader>k :m .-2<CR>==
" navigate buffer
nnoremap <M-[> :bprev<CR>
nnoremap <M-]> :bnext<CR>

" settings for gvim
:colorscheme gruvbox
:set guifont=Consolas:h16
" yank all to clipboard
set clipboard=unnamed
" list to show ALL white space characters as a character
set listchars=eol:$,tab:>�,trail:~,extends:>,precedes:<,space:_
